FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src

COPY *.sln ./

COPY c-app/*.csproj ./sourcecode/

RUN dotnet restore

COPY c-app/. ./sourcecode/

WORKDIR /src/sourcecode

RUN dotnet publish -c relese -o /app --no-restore

FROM mcr.microsoft.com/dotnet/aspnet:5.0

WORKDIR /app

COPY --from=build /app ./

ENTRYPOINT ["dotnet", "app.dll"]
